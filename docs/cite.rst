========
Citation
========

If you used BiasAway, please cite:

    - A. Khan, R. Riudavets Puig, P. Boddie, and A. Mathelier.
      BiasAway: command-line and web server to generate nucleotide
      composition-matched DNA background sequences.
      *Bioinformatics* btaa928 (2020).
      https://doi.org/10.1093/bioinformatics/btaa928
    - Worsley Hunt, R., Mathelier, A., del Peso, L. *et al.*
      Improving analysis of transcription factor binding sites within
      ChIP-Seq data based on topological motif enrichment.
      *BMC Genomics* **15**, 472 (2014).
      https://doi.org/10.1186/1471-2164-15-472
